class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  def get_move
    moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        pos =[row, col]
        moves << pos if board[pos].nil?
      end
    end

    moves.each do |move|
      if winning_move?(move)
        return move
      end
    end

    moves.sample
  end

  def winning_move?(move)
    board[move] = mark
    if board.winner != nil
      board[move] = nil
      return true
    else
      board[move] = nil
      return false
    end
  end

  def display(board)
    @board = board
  end
end
