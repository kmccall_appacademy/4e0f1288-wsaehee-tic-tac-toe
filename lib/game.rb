require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :player_one, :player_two, :current_player, :board

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    player_one.mark, player_two.mark = :X, :O
    @board = Board.new
    @current_player = player_one
  end

  def play_turn
    input = current_player.get_move
    @board.place_mark(input, current_player.mark)
    switch_players!
  end

  def play
    until @board.over?
      play_turn
    end
  end
  
  def switch_players!
    @current_player = @player_two
  end
end
