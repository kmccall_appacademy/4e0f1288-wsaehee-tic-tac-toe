class Board
  attr_accessor :grid
  def initialize(grid = Array.new(3) { Array.new(3) })
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos] == nil
  end

  def winner
    paths = [
      [[0, 0], [0, 1], [0, 2]],
      [[1, 0], [1, 1], [1, 2]],
      [[2, 0], [2, 1], [2, 2]],
      [[0, 0], [1, 0], [2, 0]],
      [[0, 1], [1, 1], [2, 1]],
      [[0, 2], [1, 2], [2, 2]],
      [[0, 0], [1, 1], [2, 2]],
      [[0, 2], [1, 1], [2, 0]]
    ]

    paths.each do |path|
      squares = path.map do |pos|
        self[pos]
      end

      return :X if squares == [:X, :X, :X]
      return :O if squares == [:O, :O, :O]

    end

    nil
  end

  def over?
    @grid.flatten.none? { |pos| pos.nil? } || winner
  end

end


#
# 0 [0, 1, 2]
# 1 [0, 1, 2]
# 2 [0, 1, 2]
