class HumanPlayer
  attr_accessor :name, :board

  def initialize(name)
    @name = name
  end

  def get_move
    puts "where"
    user_input = gets.chomp.split(", ").map(&:to_i)
  end


  def display(board)
    @board = board
    row_one = (0..2).map do |col|
      board.empty?([0, col]) ? " " : board[[0, col]].to_s
    end
    row_two = (0..2).map do |col|
      board.empty?([1, col]) ? " " : board[[1, col]].to_s
    end

    row_three = (0..2).map do |col|
      board.empty?([2, col]) ? " " : board[[2, col]].to_s
    end

    puts row_one.join(" | ")
    puts "----------------"
    puts row_two.join(" | ")
    puts "----------------"
    puts row_three.join(" | ")
  end
end
